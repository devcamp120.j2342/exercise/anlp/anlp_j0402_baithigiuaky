"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/api";

// định nghĩa table  - chưa có data
var gEmployeeTable = $("#employee-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'id' },
        { data: 'employeeCode' },
        { data: 'employeeName' },
        { data: 'position' },
        { data: 'gender' },
        { data: 'dateOfBirth' },
        { data: 'address' },
        { data: 'phoneNumber' },
        { data: 'departmentCode' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 5, // Chỉ số cột "dateOfBirth" trong mảng columns
            render: function (data) {
                return formatDateOfBirth(data);
            }
        },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i> | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gDepartmentCode = "";
var gEmployeeId = -1;
var newEmployee = {
    employeeCode: '',
    employeeName: '',
    position: '',
    gender: '',
    dateOfBirth: '',
    address: '',
    phoneNumber: '',
    departmentCode: '',
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#employee-table").on("click", ".fa-edit", function () {
        onDeatilEmployeeClick(this); // this là button được ấn
    });
    $("#employee-table").on("click", ".fa-trash", function () {
        onDeleteEmployeeByIdClick(this); // this là button được ấn
    });
    $("#create-employee").on("click", function () {
        onCreateNewEmployeeClick();
    });
    $("#update-employee").on("click", function () {
        onSaveEmployeeClick();
    });
    // khi xác nhận xóa trên modal delete
    $("#delete-employee").on("click", function () {
        onConfirmDeleteClick();
    });
// xóa toàn bộ nhân viên
    $('#delete-all-employee').on("click", function () {
        $('#modal-delete-all-employee').modal('show');
    });
    $("#confirm-delete-all-employee").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/employee/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilEmployeeClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gEmployeeTable.row(vRowSelected).data();
    gEmployeeId = vDatatableRow.id;
    gDepartmentCode = vDatatableRow.departmentCode;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/employee/details/" + gEmployeeId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadEmployeeToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteEmployeeByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-employee').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gEmployeeTable.row(vRowSelected).data();
    gEmployeeId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "/employee/delete/" + gEmployeeId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-employee').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "employee with id: " + gEmployeeId + " was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
// hàm xử lí khi ấn cập nhật phòng ban
function onSaveEmployeeClick() {
    var vDepartmentcode = $('#input-department-code').val().trim();
    if (gDepartmentCode != vDepartmentcode) {
        $.ajax({
            url: gBASE_URL + "/employee/" + gEmployeeId + "/transfer/"
                + gDepartmentCode + "/" + vDepartmentcode,
            type: "POST",
            success: function () {
            },
            error: function (error) {
                console.assert(error.responseText);
            }
        });
    }
    var newEmployee = {
        employeeCode: $('#input-employee-code').val().trim(),
        employeeName: $('#input-employee-name').val(),
        position: $('#input-position').val().trim(),
        gender: $('#input-gender').val().trim(),
        dateOfBirth: formatDateforDB($('#input-birthday').val().trim()),
        address: $('#input-address').val(),
        phoneNumber: $('#input-phone-number').val().trim(),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "/employee/update/" + gEmployeeId,
        method: 'PUT',
        data: JSON.stringify(newEmployee),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gEmployeeId = 0;
            resetEmployeeInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewEmployeeClick() {
    var vDepartmentcode = $('#input-department-code').val().trim();
    if (vDepartmentcode == "") {
        toastCreateOrder("mã phòng ban hợp lệ")
    } else if (checkDepartmentExists(vDepartmentcode) != true) {
        toastCreateOrder("mã phòng ban hợp lệ")
    } else {
        var newEmployee = {
            employeeCode: $('#input-employee-code').val().trim(),
            employeeName: $('#input-employee-name').val(),
            position: $('#input-position').val().trim(),
            gender: $('#input-gender').val().trim(),
            dateOfBirth: $('#input-birthday').val().trim(),
            address: $('#input-address').val(),
            phoneNumber: $('#input-phone-number').val().trim(),
        };
        if (validateEmployee(newEmployee)) {
            newEmployee.dateOfBirth = formatDateforDB(newEmployee.dateOfBirth)
            $.ajax({
                url: gBASE_URL + '/employee/' + vDepartmentcode + '/create',
                method: 'POST',
                data: JSON.stringify(newEmployee),
                contentType: 'application/json',
                success: (data) => {
                    Toast.fire({
                        icon: 'success',
                        title: "employee created successfully ",
                    })
                    onPageLoading();
                    resetEmployeeInput();
                },
                error: function (error) {
                    console.assert(error.responseText);
                }
            });
        }
    }
}
// xác nhận xóa toàn bộ
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/employee/delete/all",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gEmployeeTable.clear();
    //Cập nhật data cho bảng 
    gEmployeeTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gEmployeeTable.draw();
}
// load dữ liệu vào form
function loadEmployeeToInput(paramemployee) {
    $('#input-employee-code').val(paramemployee.employeeCode);
    $('#input-employee-name').val(paramemployee.employeeName);
    $('#input-position').val(paramemployee.position);
    $('#input-gender').val(paramemployee.gender);
    $('#input-birthday').val(formatDateOfBirth(paramemployee.dateOfBirth));
    $('#input-address').val(paramemployee.address);
    $('#input-phone-number').val(paramemployee.phoneNumber);
    $('#input-department-code').val(paramemployee.departmentCode);
}
function resetEmployeeInput() {
    $('#input-employee-code').val('');
    $('#input-employee-name').val('');
    $('#input-position').val('');
    $('#input-gender').val('');
    $('#input-birthday').val('');
    $('#input-address').val('');
    $('#input-phone-number').val('');
    $('#input-department-code').val('');
}

function validateEmployee(paramObj) {
    if (paramObj.employeeCode == "") {
        toastCreateOrder("mã nhân viên")
        return false;
    } else if (paramObj.employeeName == "") {
        toastCreateOrder("tên nhân viên")
        return false;
    } else if (paramObj.position == "") {
        toastCreateOrder("chức vụ")
        return false;
    } else if (paramObj.gender == "") {
        toastCreateOrder("giới tính")
        return false;
    } else if (validateDate(paramObj.dateOfBirth)) {
        toastCreateOrder("ngày sinh theo định dạng dd/mm/yyyy")
        return false;
    } else if (paramObj.address == "") {
        toastCreateOrder("địa chỉ")
        return false;
    } else if (paramObj.phoneNumber == "") {
        toastCreateOrder("số diện thoại")
        return false;
    }
    return true;
}
function validateDate(dateInput) {
    var regex = /^(\d{2})\/(\d{2})\/(\d{4})$/;
    if (regex.test(dateInput)) {
        return false;
    } else {
        return true;
    }
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập " + paramMessage,
    })
}
function formatDateOfBirth(data) {
    // Format lại ngày sinh từ yyyy-MM-dd thành dd/MM/yyyy
    var date = new Date(data);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    // Kiểm tra và thêm số 0 vào trước ngày hoặc tháng nếu cần
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return day + '/' + month + '/' + year;
}
// Hàm chuyển đổi ngày sinh từ dd/MM/yyyy thành yyyy-MM-dd
function formatDateforDB(dateString) {
    var parts = dateString.split('/');
    var day = parts[0];
    var month = parts[1];
    var year = parts[2];

    // Kiểm tra và thêm số 0 vào trước ngày hoặc tháng nếu cần
    if (day.length === 1) {
        day = '0' + day;
    }
    if (month.length === 1) {
        month = '0' + month;
    }

    return year + '-' + month + '-' + day;
}
function checkDepartmentExists(departmentCode) {
    var result = false;
    $.ajax({
        url: gBASE_URL + '/department/check/' + departmentCode,
        type: 'GET',
        async: false, // Đặt chế độ đồng bộ
        success: function (response) {
            result = response === true; // Gán giá trị boolean từ kết quả trả về
        },
        error: function (xhr, status, error) {
            console.log('Error occurred');
        }
    });
    return result;
}