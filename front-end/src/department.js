"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/api";

// định nghĩa table  - chưa có data
var gDepartmentTable = $("#department-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'id' },
        { data: 'departmentCode' },
        { data: 'departmentName' },
        { data: 'businessField' },
        { data: 'decription' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
var newDepartment = {
    departmentCode: '',
    departmentName: '',
    businessField: '',
    decription: '',
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#department-table").on("click", ".fa-edit", function () {
        onDeatilDepartmentClick(this); // this là button được ấn
    });
    $("#department-table").on("click", ".fa-trash", function () {
        onDeleteDepartmentByIdClick(this); // this là button được ấn
    });
    $("#create-department").on("click", function () {
        onCreateNewDepartmentClick();
    });
    $("#update-department").on("click", function () {
        onSaveDepartmentClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-department').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-department').on("click", function () {
        $('#modal-delete-all-department').modal('show');
    });
    $("#confirm-delete-all-department").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/department/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilDepartmentClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gDepartmentTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/department/details/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDepartmentToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteDepartmentByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-department').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gDepartmentTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "/department/delete/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-department').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "department with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "/department/delete/all",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveDepartmentClick() {
    var newDepartment = {
        departmentCode: $('#input-department-code').val().trim(),
        departmentName: $('#input-department-name').val().trim(),
        businessField: $('#input-business-field').val().trim(),
        decription: $('#input-decription').val().trim(),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "/department/update/" + gId,
        method: 'PUT',
        data: JSON.stringify(newDepartment),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetDepartmentInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewDepartmentClick() {
    var newDepartment = {
        departmentCode: $('#input-department-code').val().trim(),
        departmentName: $('#input-department-name').val(),
        businessField: $('#input-business-field').val().trim(),
        decription: $('#input-decription').val().trim(),
    };
    if (validateDepartment(newDepartment)) {
        $.ajax({
            url: gBASE_URL + '/department/create',
            method: 'POST',
            data: JSON.stringify(newDepartment),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "Department created successfully ",
                })
                onPageLoading();
                resetDepartmentInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDepartmentTable.clear();
    //Cập nhật data cho bảng 
    gDepartmentTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gDepartmentTable.draw();
}
// load dữ liệu vào form
function loadDepartmentToInput(paramDepartment) {
    $('#input-department-code').val(paramDepartment.departmentCode);
    $('#input-department-name').val(paramDepartment.departmentName);
    $('#input-business-field').val(paramDepartment.businessField);
    $('#input-decription').val(paramDepartment.decription);
}
function resetDepartmentInput() {
    $('#input-department-code').val('');
    $('#input-department-name').val('');
    $('#input-business-field').val('');
    $('#input-decription').val('');
}

function validateDepartment(paramObj) {
    if (paramObj.departmentCode == "") {
        toastCreateOrder("mã phòng ban")
        return false;
    } else if (paramObj.loaiPizza == "") {
        toastCreateOrder("tên phòng ban")
        return false;
    } else if (paramObj.businessField == "") {
        toastCreateOrder("nghiệp vụ")
        return false;
    } else if (paramObj.decription == "") {
        toastCreateOrder("giới thiệu")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập " + paramMessage,
    })
}