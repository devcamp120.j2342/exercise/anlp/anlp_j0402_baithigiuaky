package com.devcamp.company_crud.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "department")
public class CDepartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "department_code", unique = true)
    private String departmentCode;

    @NotBlank
    @Column(name = "department_name")
    private String departmentName;

    @NotBlank
    @Column(name = "business_field")
    private String businessField;

    @NotBlank
    @Column(name = "decription")
    private String decription;

    @OneToMany(targetEntity = CEmployee.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    private List<CEmployee> employees;

    public Long getId() {
        return this.id;
    }

    public String getDepartmentCode() {
        return this.departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return this.departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getBusinessField() {
        return this.businessField;
    }

    public void setBusinessField(String businessField) {
        this.businessField = businessField;
    }

    public String getDecription() {
        return this.decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }


}
