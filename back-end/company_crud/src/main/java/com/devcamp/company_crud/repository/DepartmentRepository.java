package com.devcamp.company_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.company_crud.model.CDepartment;

@Repository
public interface DepartmentRepository extends JpaRepository<CDepartment, Long> {
    CDepartment findByDepartmentCode(String departmentCode);
}
