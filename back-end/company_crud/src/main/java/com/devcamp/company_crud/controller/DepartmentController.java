package com.devcamp.company_crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.company_crud.model.CDepartment;
import com.devcamp.company_crud.repository.DepartmentRepository;

import java.util.*;

@RestController
@RequestMapping("/api")
public class DepartmentController {
    @Autowired
    private DepartmentRepository departmentRepository;

    @CrossOrigin
    @GetMapping("/department/all")
    public ResponseEntity<List<CDepartment>> getallDepartment() {
        try {
            List<CDepartment> departments = departmentRepository.findAll();
            return ResponseEntity.ok(departments);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin
    @GetMapping("/department/details/{id}")
    public ResponseEntity<CDepartment> getDepartmentById(@PathVariable Long id) {
        try {
            Optional<CDepartment> departmentOptional = departmentRepository.findById(id);
            if (departmentOptional.isPresent()) {
                CDepartment department = departmentOptional.get();
                return ResponseEntity.ok(department);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Create department
    @CrossOrigin
    @PostMapping("/department/create")
    public ResponseEntity<Object> createDepartment(@RequestBody CDepartment cDepartment) {
        try {
            CDepartment newDepartment = new CDepartment();
            newDepartment.setDepartmentCode(cDepartment.getDepartmentCode());
            newDepartment.setDepartmentName(cDepartment.getDepartmentName());
            newDepartment.setBusinessField(cDepartment.getBusinessField());
			newDepartment.setDecription(cDepartment.getDecription());
			CDepartment savedRole = departmentRepository.save(newDepartment);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified department: " + e.getCause().getCause().getMessage());
        }
    }

    @CrossOrigin
    @PutMapping("/department/update/{id}")
    public ResponseEntity<CDepartment> updateDepartment(@PathVariable("id") Long id,
            @RequestBody CDepartment department) {
        try {
            Optional<CDepartment> newDepartmentData = departmentRepository.findById(id);
            if (newDepartmentData.isPresent()) {
                CDepartment newDepartment = newDepartmentData.get();
                if(department.getDepartmentCode() != null){
					newDepartment.setDepartmentCode(department.getDepartmentCode());
				}
				if(department.getDepartmentName() != null){
					newDepartment.setDepartmentName(department.getDepartmentName());
				}
				if(department.getBusinessField() != null){
					newDepartment.setBusinessField(department.getBusinessField());
				}
				if(department.getDecription() != null){
					newDepartment.setDecription(department.getDecription());
				}
                CDepartment saveDepartment = departmentRepository.save(newDepartment);
                return new ResponseEntity<>(saveDepartment, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Delete department
    @CrossOrigin
    @DeleteMapping("/department/delete/{id}")
    public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
        try {
            Optional<CDepartment> deleteDepartment = departmentRepository.findById(id);
            if (deleteDepartment.isPresent()) {
                departmentRepository.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get department detail by code
    @CrossOrigin
    @GetMapping("/department/code/{code}")
    public ResponseEntity<CDepartment> getDepartmentByCode(@PathVariable String code) {
        try {
            CDepartment existingDepartment = departmentRepository.findByDepartmentCode(code);
            if (existingDepartment != null) {
                return ResponseEntity.ok(existingDepartment);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    // xóa toàn bộ department
    @CrossOrigin
    @DeleteMapping("/department/delete/all")
    public ResponseEntity<String> deleteAllDepartment() {
        try {
            departmentRepository.deleteAll();
            return ResponseEntity.ok("All department have been deleted.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete department.");
        }
    }

    // check department có tồn tại không dựa theo departmentcode
    @CrossOrigin
    @GetMapping("/department/check/{departmentCode}")
    public ResponseEntity<Boolean> checkDepartmentExists(@PathVariable String departmentCode) {
        try {
            CDepartment department = departmentRepository.findByDepartmentCode(departmentCode);
            boolean exists = (department != null);
            return ResponseEntity.ok(exists);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
}