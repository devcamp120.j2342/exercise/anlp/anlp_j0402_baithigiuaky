package com.devcamp.company_crud.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.company_crud.model.CDepartment;
import com.devcamp.company_crud.model.CEmployee;
import com.devcamp.company_crud.repository.DepartmentRepository;
import com.devcamp.company_crud.repository.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {
    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @CrossOrigin
    @GetMapping("/employee/all")
    public ResponseEntity<List<Map<String, Object>>> getAllEmployees() {
        try {
            List<CEmployee> employees = employeeRepository.findAll();
            List<Map<String, Object>> response = new ArrayList<>();

            for (CEmployee employee : employees) {
                CDepartment department = departmentRepository.findById(employee.getDepartment().getId()).orElse(null);
                if (department != null) {
                    Map<String, Object> employeeData = new HashMap<>();
                    employeeData.put("id", employee.getId());
                    employeeData.put("employeeCode", employee.getEmployeeCode());
                    employeeData.put("employeeName", employee.getEmployeeName());
                    employeeData.put("position", employee.getPosition());
                    employeeData.put("gender", employee.getGender());
                    employeeData.put("dateOfBirth", employee.getDateOfBirth());
                    employeeData.put("address", employee.getAddress());
                    employeeData.put("phoneNumber", employee.getPhoneNumber());
                    employeeData.put("departmentCode", department.getDepartmentCode());
                    response.add(employeeData);
                }
            }
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin
    @GetMapping("/employee/details/{id}")
    public ResponseEntity<Map<String, Object>> getEmployeeById(@PathVariable Long id) {
        try {
            Optional<CEmployee> employeeOptional = employeeRepository.findById(id);
            if (employeeOptional.isPresent()) {
                CEmployee employee = employeeOptional.get();
                String departmentCode = employee.getDepartment().getDepartmentCode();
                Map<String, Object> employeeData = new HashMap<>();
                employeeData.put("id", employee.getId());
                employeeData.put("employeeCode", employee.getEmployeeCode());
                employeeData.put("employeeName", employee.getEmployeeName());
                employeeData.put("position", employee.getPosition());
                employeeData.put("gender", employee.getGender());
                employeeData.put("dateOfBirth", employee.getDateOfBirth());
                employeeData.put("address", employee.getAddress());
                employeeData.put("phoneNumber", employee.getPhoneNumber());
                employeeData.put("departmentCode", departmentCode);
                return ResponseEntity.ok(employeeData);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // get all eloyee by department_code
    @CrossOrigin
    @GetMapping("/employee/{department_code}/all")
    public ResponseEntity<List<CEmployee>> getEmployeesByDepartmentId(@PathVariable("department_code") String code) {
        try {
            CDepartment departmentData = departmentRepository.findByDepartmentCode(code);
            Long departmentId = departmentData.getId();
            List<CEmployee> employees = employeeRepository.findByDepartmentId(departmentId);
            if (!employees.isEmpty()) {
                return ResponseEntity.ok(employees);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Create employee by department_code
    @CrossOrigin
    @PostMapping("employee/{department_code}/create")
    public ResponseEntity<Object> createEmployeeByDepartmentCode(@PathVariable("department_code") String code,
            @RequestBody CEmployee cEmployee) {
        try {
            CDepartment departmentData = departmentRepository.findByDepartmentCode(code);
            if (departmentData != null) {
                CEmployee newEmployee = new CEmployee();
                newEmployee.setEmployeeCode(cEmployee.getEmployeeCode());
                newEmployee.setEmployeeName(cEmployee.getEmployeeName());
                newEmployee.setPosition(cEmployee.getPosition());
                newEmployee.setGender(cEmployee.getGender());
                newEmployee.setDateOfBirth(cEmployee.getDateOfBirth());
                newEmployee.setAddress(cEmployee.getAddress());
                newEmployee.setPhoneNumber(cEmployee.getPhoneNumber());
                newEmployee.setDepartment(departmentData);
                CEmployee savedRole = employeeRepository.save(newEmployee);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Update employee by id
    @CrossOrigin
    @PutMapping("/employee/update/{id}")
    public ResponseEntity<CEmployee> updateDepartment(@PathVariable("id") Long id,
            @RequestBody CEmployee updatEmployee) {
        try {
            Optional<CEmployee> EmployeeData = employeeRepository.findById(id);
            if (EmployeeData.isPresent()) {
                CEmployee newEmployee = EmployeeData.get();
                if (updatEmployee.getEmployeeName() != null) {
                    newEmployee.setEmployeeName(updatEmployee.getEmployeeName());
                }
                if (updatEmployee.getGender() != null) {
                    newEmployee.setGender(updatEmployee.getGender());
                }
                if (updatEmployee.getDateOfBirth() != null) {
                    newEmployee.setDateOfBirth(updatEmployee.getDateOfBirth());
                }
                if (updatEmployee.getAddress() != null) {
                    newEmployee.setAddress(updatEmployee.getAddress());
                }
                if (updatEmployee.getPhoneNumber() != null) {
                    newEmployee.setPhoneNumber(updatEmployee.getPhoneNumber());
                }
                if (updatEmployee.getDepartment() != null) {
                    newEmployee.setDepartment(updatEmployee.getDepartment());
                }
                CEmployee saveEmployee = employeeRepository.save(newEmployee);
                return new ResponseEntity<>(saveEmployee, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Delete employee
    @CrossOrigin
    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Void> deleteteEmployee(@PathVariable Long id) {
        try {
            Optional<CEmployee> deleteEmployee = employeeRepository.findById(id);
            if (deleteEmployee.isPresent()) {
                employeeRepository.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @DeleteMapping("/employee/delete/all")
    public ResponseEntity<String> deleteAllEmployees() {
        try {
            employeeRepository.deleteAll();
            return ResponseEntity.ok("All employees have been deleted.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete employees.");
        }
    }

    @CrossOrigin
    @PostMapping("/employee/{employeeId}/transfer/{oldDepartmentCode}/{newDepartmentCode}")
    public ResponseEntity<String> transferEmployee(@PathVariable Long employeeId,
            @PathVariable String oldDepartmentCode, @PathVariable String newDepartmentCode) {
        try {
            CEmployee employee = employeeRepository.findById(employeeId).orElse(null);
            if (employee == null) {
                return ResponseEntity.notFound().build();
            }

            CDepartment oldDepartment = departmentRepository.findByDepartmentCode(oldDepartmentCode);
            CDepartment newDepartment = departmentRepository.findByDepartmentCode(newDepartmentCode);

            if (oldDepartment == null || newDepartment == null || oldDepartment != employee.getDepartment()) {
                return ResponseEntity.badRequest().body("Invalid department code");
            }

            employee.setDepartment(newDepartment);
            employeeRepository.save(employee);

            return ResponseEntity.ok("Employee transferred successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
