package com.devcamp.company_crud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.company_crud.model.CEmployee;

@Repository
public interface EmployeeRepository extends JpaRepository<CEmployee,Long> {
    List<CEmployee> findByDepartmentId(Long departmentId);
}
