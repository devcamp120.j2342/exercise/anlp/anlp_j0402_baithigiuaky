-- Chèn dữ liệu mẫu cho bảng Department
INSERT INTO Department (department_code, department_name, business_field, decription)
VALUES
    ('SL01', 'Sales', 'Marketing', 'Sales department handles marketing and sales activities.'),
    ('IT02', 'IT', 'Technology', 'IT department manages the company''s technology infrastructure.'),
    ('HR02', 'HR', 'Human Resources', 'HR department handles employee recruitment and management.');

-- Chèn dữ liệu mẫu cho bảng Employee
INSERT INTO Employee (employee_code, employee_name, position, gender, date_of_birth, address, phone_number, department_id)
VALUES
    ('E001', 'Hau', 'Manager', 'Male', '1990/05/15', '123 ABC Street, HCM', '1234567890', '1'),
    ('E002', 'Huy', 'Engineer', 'Female', '1992/08/20', '456 XYZ Street, HCM', '9876543210', '1'),
    ('E003', 'Trinh', 'Sales Representative', 'Male', '1995/02/10', '789 DEF Street, HCM', '4567890123', '1'),
    ('E004', 'Y', 'Sales Representative', 'Female', '1993/09/25', '321 GHI Street, HCM', '7890123456', '1'),
    ('E005', 'Tin', 'Sales Manager', 'Male', '1988/07/12', '567 JKL Street, Hanoi', '9012345678', '1'),
    ('E006', 'Mai', 'IT Specialist', 'Female', '1991/03/18', '987 MNO Street, Hanoi', '3456789012', '2'),
    ('E007', 'Khoi', 'IT Engineer', 'Male', '1994/11/05', '654 PQR Street, Hanoi', '7890123454', '2'),
    ('E008', 'Viet', 'IT Support Specialist', 'Female', '1993/06/22', '789 STU Street, Hanoi', '9012345675', '2'),
    ('E009', 'Thinh', 'IT Manager', 'Male', '1989/04/01', '123 VWX Street, Da Nang', '2345678901', '2'),
    ('E010', 'Tuan', 'HR Coordinator', 'Female', '1992/10/08', '987 YZ Street, HCM', '5678901233', '3'),
    ('E011', 'An', 'HR Specialist', 'Male', '1994/12/20', '321 AB Street, HCM', '8901234567', '3'),
    ('E012', 'Hieu', 'HR Manager', 'Female', '1987/08/04', '654 CD Street, HCM', '2345678801', '3'),
    ('E013', 'Minh', 'HR Assistant', 'Male', '1996/01/30', '789 EF Street, HCM', '5678901234', '3');
